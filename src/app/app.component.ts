import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  public user: User;

  constructor(private http: HttpClient) {}

  ngOnInit(){
    this.http.get('./assets/users.json').subscribe((data:User) => this.user = data)
  }
}
